package mk.ukim.finki.foodmarketmap.foodmarketmap.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReceiptUser {

    @Id
    public String id;
}
