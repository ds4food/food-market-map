package mk.ukim.finki.foodmarketmap.foodmarketmap.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FcdbItem {

    @Id
    public String id;

    public String name;

    public String nutriScore;

    public Boolean vegan;

    public Boolean bio;

    public Double energy;

    public Double proteins;

    public Double fat;

    public Double carbohydrates;

    public Double salt;


}
