package mk.ukim.finki.foodmarketmap.foodmarketmap.service.impl;

import mk.ukim.finki.foodmarketmap.foodmarketmap.model.FcdbItem;
import mk.ukim.finki.foodmarketmap.foodmarketmap.model.Receipt;
import mk.ukim.finki.foodmarketmap.foodmarketmap.model.ReceiptItem;
import mk.ukim.finki.foodmarketmap.foodmarketmap.model.ReceiptUser;
import mk.ukim.finki.foodmarketmap.foodmarketmap.service.ReceiptServiceFacade;
import mk.ukim.finki.foodmarketmap.foodmarketmap.web.dto.ReceiptDto;
import mk.ukim.finki.foodmarketmap.foodmarketmap.web.dto.ReceiptItemDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Random;

@Service
public class ReceiptServiceFacadeMockImpl implements ReceiptServiceFacade {
    @Override
    public Page<ReceiptDto> list() {
        return new PageImpl<>(Arrays.asList(createReceiptDto(), createReceiptDto(), createReceiptDto()));
    }

    @Override
    public ReceiptDto uploadReceipt(String uid, MultipartFile image) {
        return createReceiptDto();
    }

    @Override
    public ReceiptDto uploadAdditionalReceiptImage(String uid, Long receiptId, MultipartFile image) {
        return createReceiptDto();
    }

    @Override
    public ReceiptItem updateItem(String uid, Long id, String name, Double quantity, Double price) {
        return creatItem();
    }

    @Override
    public ReceiptItem updateItemMappingWithFcdbId(String uid, Long id, String fcdbId) {
        return creatItem();
    }

    @Override
    public ReceiptItem updateItemMappingByBarcode(String uid, Long id, String barcode) {
        return creatItem();
    }

    @Override
    public ReceiptItem updateItemMappingByRawFoodImage(String uid, Long id, MultipartFile image) {
        return creatItem();
    }

    private ReceiptItem creatItem() {
        Random random = new Random();

        return ReceiptItem.builder()
                .id(1L)
                .receipt(
                        Receipt.builder()
                                .id(1L)
                                .merchant("Hoffer")
                                .user(new ReceiptUser("userId")) // replace "userId" with actual user id
                                .total(100.0) // replace with actual total
                                .shoppingTime(LocalDateTime.now()) // replace with actual shopping time
                                .uploadedAt(LocalDateTime.now()) // replace with actual upload time
                                .build()
                )
                .fcdbItem(FcdbItem.builder()
                        .id(String.valueOf(random.nextInt()))
                        .name("randomName1")
                        .nutriScore("A")
                        .vegan(random.nextBoolean())
                        .bio(random.nextBoolean())
                        .energy(random.nextDouble())
                        .proteins(random.nextDouble())
                        .fat(random.nextDouble())
                        .carbohydrates(random.nextDouble())
                        .salt(random.nextDouble())
                        .build())
                .name("itemName1")
                .quantity(1.0)
                .price(100.0)
                .discount(0.0)
                .specialPrice(100.0)
                .build();
    }

    private ReceiptDto createReceiptDto() {

        Random random = new Random();

        return ReceiptDto.builder()
                .id(1L)
                .merchant("Hoffer")
                .user(new ReceiptUser("userId")) // replace "userId" with actual user id
                .total(100.0) // replace with actual total
                .shoppingTime(LocalDateTime.now()) // replace with actual shopping time
                .uploadedAt(LocalDateTime.now()) // replace with actual upload time
                .items(Arrays.asList(
                        ReceiptItemDto.builder()
                                .id(1L)
                                .receiptId(1L)
                                .fcdbItem(FcdbItem.builder()
                                        .id(String.valueOf(random.nextInt()))
                                        .name("randomName1")
                                        .nutriScore("A")
                                        .vegan(random.nextBoolean())
                                        .bio(random.nextBoolean())
                                        .energy(random.nextDouble())
                                        .proteins(random.nextDouble())
                                        .fat(random.nextDouble())
                                        .carbohydrates(random.nextDouble())
                                        .salt(random.nextDouble())
                                        .build())
                                .name("itemName1")
                                .quantity(1.0)
                                .price(100.0)
                                .discount(0.0)
                                .specialPrice(100.0)
                                .build(),
                        ReceiptItemDto.builder()
                                .id(2L)
                                .receiptId(1L)
                                .fcdbItem(FcdbItem.builder()
                                        .id(String.valueOf(random.nextInt()))
                                        .name("randomName2")
                                        .nutriScore("B")
                                        .vegan(random.nextBoolean())
                                        .bio(random.nextBoolean())
                                        .energy(random.nextDouble())
                                        .proteins(random.nextDouble())
                                        .fat(random.nextDouble())
                                        .carbohydrates(random.nextDouble())
                                        .salt(random.nextDouble())
                                        .build())
                                .name("itemName2")
                                .quantity(2.0)
                                .price(200.0)
                                .discount(0.0)
                                .specialPrice(200.0)
                                .build(),
                        ReceiptItemDto.builder()
                                .id(3L)
                                .receiptId(1L)
                                .fcdbItem(FcdbItem.builder()
                                        .id(String.valueOf(random.nextInt()))
                                        .name("randomName3")
                                        .nutriScore("C")
                                        .vegan(random.nextBoolean())
                                        .bio(random.nextBoolean())
                                        .energy(random.nextDouble())
                                        .proteins(random.nextDouble())
                                        .fat(random.nextDouble())
                                        .carbohydrates(random.nextDouble())
                                        .salt(random.nextDouble())
                                        .build())
                                .name("itemName3")
                                .quantity(3.0)
                                .price(300.0)
                                .discount(0.0)
                                .specialPrice(300.0)
                                .build()
                ))
                .build();
    }
}
