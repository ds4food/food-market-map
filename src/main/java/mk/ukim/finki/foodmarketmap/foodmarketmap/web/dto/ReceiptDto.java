package mk.ukim.finki.foodmarketmap.foodmarketmap.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import mk.ukim.finki.foodmarketmap.foodmarketmap.model.ReceiptUser;

import java.time.LocalDateTime;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReceiptDto {

    public Long id;

    public String merchant;

    public ReceiptUser user;

    public Double total;

    public LocalDateTime shoppingTime;

    public LocalDateTime uploadedAt;

    public List<ReceiptItemDto> items;
}
