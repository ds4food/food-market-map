package mk.ukim.finki.foodmarketmap.foodmarketmap.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReceiptItem {

    @Id
    @GeneratedValue
    public Long id;

    @ManyToOne
    public Receipt receipt;

    @ManyToOne
    public FcdbItem fcdbItem;

    public String name;

    public Double quantity;

    public Double price;

    public Double discount;

    public Double specialPrice;
}
