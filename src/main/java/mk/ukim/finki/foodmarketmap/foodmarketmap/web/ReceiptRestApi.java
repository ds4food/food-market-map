package mk.ukim.finki.foodmarketmap.foodmarketmap.web;

import lombok.AllArgsConstructor;
import mk.ukim.finki.foodmarketmap.foodmarketmap.model.FcdbItem;
import mk.ukim.finki.foodmarketmap.foodmarketmap.model.ReceiptItem;
import mk.ukim.finki.foodmarketmap.foodmarketmap.model.ReceiptUser;
import mk.ukim.finki.foodmarketmap.foodmarketmap.service.ReceiptServiceFacade;
import mk.ukim.finki.foodmarketmap.foodmarketmap.web.dto.ReceiptDto;
import mk.ukim.finki.foodmarketmap.foodmarketmap.web.dto.ReceiptItemDto;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Random;

@AllArgsConstructor
@RestController
@RequestMapping(produces = "application/json")
public class ReceiptRestApi {

    private final ReceiptServiceFacade serviceFacade;

    @GetMapping
    public Page<ReceiptDto> list() {
        return serviceFacade.list();
    }

    @PostMapping("/upload")
    public ReceiptDto uploadReceipt(
            @RequestHeader String uid,
            MultipartFile image) {

        return serviceFacade.uploadReceipt(uid, image);

    }

    @PostMapping("/upload/{receiptId}")
    public ReceiptDto uploadAdditionalReceiptImage(
            @RequestHeader String uid,
            @PathVariable Long receiptId,
            MultipartFile image) {

        return serviceFacade.uploadAdditionalReceiptImage(uid, receiptId, image);
    }

    @PostMapping("/update/item/{id}")
    public ReceiptItem updateItem(
            @RequestHeader String uid,
            @PathVariable Long id,
            @RequestParam String name,
            @RequestParam Double quantity,
            @RequestParam Double price) {

        return serviceFacade.updateItem(uid, id, name, quantity, price);
    }

    @PostMapping("/update/item/{id}/fcdb_id")
    public ReceiptItem updateItemMappingWithFcdbId(
            @RequestHeader String uid,
            @PathVariable Long id,
            @RequestParam String fcdbId) {

        return serviceFacade.updateItemMappingWithFcdbId(uid, id, fcdbId);
    }

    @PostMapping("/update/item/{id}/barcode")
    public ReceiptItem updateItemMappingByBarcode(
            @RequestHeader String uid,
            @PathVariable Long id,
            @RequestParam String barcode) {

        return serviceFacade.updateItemMappingByBarcode(uid, id, barcode);
    }

    @PostMapping("/update/item/{id}/image")
    public ReceiptItem updateItemMappingByRawFoodImage(
            @RequestHeader String uid,
            @PathVariable Long id,
            MultipartFile image) {

        return serviceFacade.updateItemMappingByRawFoodImage(uid, id, image);
    }


}
