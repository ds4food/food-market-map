package mk.ukim.finki.foodmarketmap.foodmarketmap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodMarketMapApplication {

    public static void main(String[] args) {
        SpringApplication.run(FoodMarketMapApplication.class, args);
    }

}
