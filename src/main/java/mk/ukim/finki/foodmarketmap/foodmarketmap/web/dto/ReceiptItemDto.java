package mk.ukim.finki.foodmarketmap.foodmarketmap.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import mk.ukim.finki.foodmarketmap.foodmarketmap.model.FcdbItem;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReceiptItemDto {

    public Long id;

    public Long receiptId;

    public FcdbItem fcdbItem;

    public String name;

    public Double quantity;

    public Double price;

    public Double discount;

    public Double specialPrice;
}
