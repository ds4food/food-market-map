package mk.ukim.finki.foodmarketmap.foodmarketmap.service;

import mk.ukim.finki.foodmarketmap.foodmarketmap.model.ReceiptItem;
import mk.ukim.finki.foodmarketmap.foodmarketmap.web.dto.ReceiptDto;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

public interface ReceiptServiceFacade {

    Page<ReceiptDto> list();

    ReceiptDto uploadReceipt(String uid, MultipartFile image);

    ReceiptDto uploadAdditionalReceiptImage(String uid, Long receiptId, MultipartFile image);

    ReceiptItem updateItem(String uid, Long id, String name, Double quantity, Double price);

    ReceiptItem updateItemMappingWithFcdbId(String uid, Long id, String fcdbId);

    ReceiptItem updateItemMappingByBarcode(String uid, Long id, String barcode);

    ReceiptItem updateItemMappingByRawFoodImage(String uid, Long id, MultipartFile image);
}
