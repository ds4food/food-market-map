package mk.ukim.finki.foodmarketmap.foodmarketmap.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReceiptImage {

    @Id
    @GeneratedValue
    public Long id;

    @ManyToOne
    public Receipt receipt;

    public byte[] image;

    @Column(length = 10_000)
    public String imageText;
}
