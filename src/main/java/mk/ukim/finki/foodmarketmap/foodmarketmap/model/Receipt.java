package mk.ukim.finki.foodmarketmap.foodmarketmap.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Receipt {

    @Id
    @GeneratedValue
    public Long id;

    public String merchant;

    @ManyToOne
    public ReceiptUser user;

    public Double total;

    public LocalDateTime shoppingTime;

    public LocalDateTime uploadedAt;
}
